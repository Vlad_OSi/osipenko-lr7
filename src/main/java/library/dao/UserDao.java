package library.dao;

import library.model.accounts.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {


    private final SessionFactory sessionFactory;

    public UserDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public User getByEmail(String email) {
        return (User) getSession().createQuery("select u from User u where u.email = :email")
                .setParameter("email", email)
                .uniqueResult();
    }
}
