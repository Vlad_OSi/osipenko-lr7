package library.dao;


import library.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookDao {


    private final SessionFactory sessionFactory;

    @Autowired
    public BookDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void updateOrInsert(Book book) {
        getSession().saveOrUpdate(book);
    }

    public List<Book> getAll() {
        return getSession().createQuery("SELECT b FROM Book b")
                .getResultList();
    }

    public List<Book> getByFields(String firstParam, String first) {

        return getSession().createQuery("SELECT b FROM Book b WHERE :field like :sValue")
                .setParameter("field", "b."+firstParam)
                .setParameter("sValue", "%" + first.toLowerCase() + "%")
                .getResultList();
    }

    public Book getById(int id) {
        return getSession().get(Book.class, id);
    }

    public void delete(int id) {
        Book book = new Book();
        book.setId(id);
        getSession().delete(book);
    }
}
