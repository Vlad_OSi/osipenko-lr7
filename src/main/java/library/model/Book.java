package library.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "books")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book implements Comparable<Book> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @ManyToOne(targetEntity = Genre.class)
    @JoinColumn(referencedColumnName = "id", name = "genre_id")
    private Genre genre;

    @Size(min=2,max=200,message = "Author should contains from 2 to 200 chars")
    @Column(name = "author")
    private String author;

    @NotNull
    @Min(value = 0, message = "Min is 0")
    @Max(value = 5, message = "Max for mark is 5")
    @Column(name = "mark")
    private int mark;



    public Book(int mark) {
        this.id = id;

    }

    @Override
    public boolean equals(Object obj) {
        return id == ((Book) obj).id;
    }

    @Override
    public String toString() {
        return id + "   " + name + "   " + genre + "   " + author + " " + mark + "  ";
    }


    @Override
    public int compareTo(Book o) {
        return id - o.id;
    }
}
