package library.serices;

import library.dao.GenreDao;
import library.model.Genre;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GenreService {

    private final GenreDao genreDao;

    public GenreService(GenreDao genreDao) {
        this.genreDao = genreDao;
    }


    @Transactional(readOnly = true)
    public Genre getById(int id) {
        return genreDao.getById(id);
    }

    @Transactional
    public List<Genre> getAll() {
        return genreDao.getAll();
    }

    @Transactional
    public void delete(int id) {
        genreDao.delete(id);
    }

    @Transactional
    public void createOrUpdate(Genre genre) {
        genreDao.updateOrCreate(genre);
    }

}
