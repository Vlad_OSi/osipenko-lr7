package library.rest.controllers;

import library.model.Book;
import library.serices.BookService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookControllerRest {
    private final BookService bookService;

    public BookControllerRest(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/api/books")
    public @ResponseBody
    List<Book> get() {
        return bookService.getAll();
    }
}
