package library.rest.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
public class AgeControllerRest {

    @GetMapping("/api/birth/{day}")
    public @ResponseBody
    ResultDto find(@PathVariable String day) {
        ResultDto resultDto = new ResultDto();
        try {
            LocalDate birth = LocalDate.parse(day);
            LocalDate now = LocalDate.now();
            int age = 0;
            for(;birth.isBefore(now);birth =birth.plusYears(1)){
                age++;
            }
            resultDto.setResult(Integer.toString(age-1));
        } catch (Exception e) {
            resultDto.setResult("Error");
        }
        return resultDto;
    }
}
