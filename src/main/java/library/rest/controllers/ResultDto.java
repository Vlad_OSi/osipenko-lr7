package library.rest.controllers;

import lombok.Data;
import lombok.Setter;
import lombok.ToString;

@Data
@ToString
public class ResultDto {

    private String result;
}
