package library.soap.publisher;

import library.soap.AgeControllerImpl;

import javax.xml.ws.Endpoint;

public class SoapPublisher {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9999/ws/find", new AgeControllerImpl());
    }

}
