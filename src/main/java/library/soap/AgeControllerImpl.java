package library.soap;

import library.rest.controllers.ResultDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.time.LocalDate;
import java.util.concurrent.ExecutionException;

@WebService(endpointInterface = "library.soap.AgeController", name = "AgeController")
public class AgeControllerImpl implements AgeController {

    @WebMethod(action = "/age")
    public ResultDto find(@WebParam String localDate) throws ExecutionException {
        ResultDto resultDto = new ResultDto();
        try {
            LocalDate birth = LocalDate.parse(localDate);
            LocalDate now = LocalDate.now();
            int age = 0;
            System.out.println();
            for(;birth.isBefore(now);birth = birth.plusYears(1)){
                System.out.println();
                age++;
            }
            resultDto.setResult(Integer.toString(age-1));
        } catch (Exception e) {
            resultDto.setResult("Error");
        }
        return resultDto;
    }
}
