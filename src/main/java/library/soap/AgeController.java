package library.soap;

import library.rest.controllers.ResultDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.time.LocalDate;
import java.util.concurrent.ExecutionException;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface AgeController {
    @WebMethod(action = "/age")
    ResultDto find(@WebParam String localDate) throws ExecutionException;
}
