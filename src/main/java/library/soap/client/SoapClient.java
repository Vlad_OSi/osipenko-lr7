package library.soap.client;

import library.soap.AgeController;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public class SoapClient {

    public static void main(String[] args) throws Exception {

        URL url = new URL("http://localhost:9999/ws/find?wsdl");
        QName qname = new QName("http://soap.library/", "AgeControllerImplService");

        Service service = Service.create(url, qname);
        qname = new QName("http://soap.library/", "AgeControllerPort");
        AgeController controller = service.getPort(qname,AgeController.class);

        System.out.println(controller.find("2000-12-10").getResult());
        System.out.println(controller.find("1982-10-04").getResult());
    }
}
