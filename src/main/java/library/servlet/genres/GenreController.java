package library.servlet.genres;

import library.model.Book;
import library.model.Genre;
import library.serices.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/genres")
public class GenreController {

    private final GenreService genreService;

    public GenreController(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping
    public String get(Model model) {
        List<Genre> genres = genreService.getAll();
        model.addAttribute("genres", genres);
        return "/genres.jsp";
    }

    @GetMapping("/update/{id}")
    public String update(Model model, @PathVariable Integer id) {
        List<Genre> genres = genreService.getAll();
        model.addAttribute("genres", genres);
        Genre genre = genreService.getById(id);
        model.addAttribute("genre", genre);
        return "/genre-update.jsp";

    }

    @PostMapping("/update/{id}")
    public String update(Model model, @PathVariable Integer id, @ModelAttribute Genre genre) {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Genre>> constraintViolations = validator.validate(genre);
        if (constraintViolations.size() > 0) {
            model.addAttribute("validation", constraintViolations.stream().map(s -> s.getMessage()).collect(Collectors.toSet()));
            return "/genre-update.jsp";
        }
        genre.setId(id);
        genreService.createOrUpdate(genre);
        return "redirect: /osipenko_library_302_war/genres";
    }

    @GetMapping("/delete/{id}")
    public String delete(Model model, @PathVariable Integer id) {
        genreService.delete(id);
        return "redirect: /osipenko_library_302_war/genres";
    }

    @PostMapping("/create")
    protected String create(Model model, @ModelAttribute Genre genre) {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Genre>> constraintViolations = validator.validate(genre);
        if (constraintViolations.size() > 0) {
            model.addAttribute("genre", genre);
            model.addAttribute("validation", constraintViolations.stream().map(s->s.getMessage()).collect(Collectors.toSet()));
           return "/genre-create.jsp";
        }
            genreService.createOrUpdate(genre);
            return "redirect: /osipenko_library_302_war/genres";

    }

    @GetMapping("/create")
    protected String create(Model model) {
        return "/genre-create.jsp";
    }
}
