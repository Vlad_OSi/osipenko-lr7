/*
package library.servlet.genres;

import library.dao.jdbc.GenreDao;
import library.model.Genre;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "GenreListServlet", urlPatterns = {"/old/genres"})
public class GenreListServlet extends HttpServlet {

    @Inject
    private GenreDao genreDao;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Genre> genres = genreDao.readGenres();
            request.setAttribute("genres", genres);

            getServletContext().getRequestDispatcher("/genres.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
*/
