/*
package library.servlet.books;

import library.dao.jdbc.BookDao;
import library.model.Book;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


@WebServlet(urlPatterns = {"/old/books"})
public class BookListServlet extends javax.servlet.http.HttpServlet {
    @Inject
    private BookDao bookDao;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        try {
            String value = request.getParameter("value");
            List<Book> books;
            if (value == null || value.isEmpty()) {
                books = bookDao.readBooks();
            } else {
                books = bookDao.readBooksByFields(request.getParameter("type"), value);
            }

            request.setAttribute("books", books);

            getServletContext().getRequestDispatcher("/list.jsp").forward(request, response);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
*/
