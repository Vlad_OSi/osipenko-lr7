<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add book</title>
</head>
<body>
<a href="/osipenko_library_302_war">Main</a>
<p>${validation}</p><br/>
<form action="" method="post">
    <table align="center">
        <tr><td>Name</td><td>
    <input type="text" name="name" value="${book.getName()}" required></td></tr>
        <tr><td>Author</td><td>
    <input type="text" name="author" value="${book.getAuthor()}" required></td></tr>
        <tr><td>Genre</td><td>
    <select name="genre.id" required>

        <c:forEach items="${genres}" var="genre">
            <c:choose>
                <c:when test="${genre.getId() == book.getGenre().getId()}">
                    <option value="${genre.getId()}" selected>${genre.getName()}</option>
                </c:when>
                <c:otherwise>
                    <option value="${genre.getId()}">${genre.getName()}</option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </select></td></tr>
        <tr><td>Mark</td><td>
    <input type="number" min="1" max="5" name="mark" value="${book.getMark()}" required></td></tr>
        <tr><td></td><td>
    <input type="submit" value="Create"></td></tr>
    </table>
</form>
</body>
</html>
